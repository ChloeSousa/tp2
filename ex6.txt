package aula3;

import java.util.Scanner;
public class ex6 {

	public static void main(String[] args) {
		
		System.out.println ("Introduza as medidas dos lados");
		Scanner entrada = new Scanner(System.in);
		int lado1 = entrada.nextInt();
		int lado2 = entrada.nextInt();
		
		int area = lado1*lado2;
		
		if (lado1 == lado2) {
			System.out.println ("Área do quadrado = " + area);
		}
		
		else{
			System.out.println ("Área do rectângulo = " + area);
		}
		
	}
}